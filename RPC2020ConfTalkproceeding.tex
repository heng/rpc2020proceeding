\documentclass[a4paper,11pt]{article}
\pdfoutput=1 % if your are submitting a pdflatex (i.e. if you have
             % images in pdf, png or jpg format)

\usepackage{jinstpub} % for details on the use of the package, please
                     % see the JINST-author-manual
\usepackage{siunitx}
\usepackage{lineno}
\usepackage{subfig}

\linenumbers

\input{commands.tex}

\title{\boldmath Performance of the ATLAS Resistive Plate Chamber detector and Level-1 muon barrel trigger at $\sqrt{s}$ = 13 TeV}


%% %simple case: 2 authors, same institution
\author{H.Li}
%\author{and A. Nother Author}
\affiliation{Department of Modern Physics and State Key Laboratory of Particle Detection and Electronics, University of Science and Technology of China\\96 Jinzhai Road, Hefei 230026, Anhui China}

% more complex case: 4 authors, 3 institutions, 2 footnotes
%\author[a,b,1]{F. Irst,\note{Corresponding author.}}
%\author[c]{S. Econd,}
%\author[a,2]{T. Hird\note{Also at Some University.}}
%\author[c,2]{and Fourth}

% The "\note" macro will give a warning: "Ignoring empty anchor..."
% you can safely ignore it.

%\affiliation[a]{One University,\\some-street, Country}
%\affiliation[b]{Another University,\\different-address, Country}
%\affiliation[c]{A School for Advanced Studies,\\some-location, Country}

% e-mail addresses: only for the forresponding author
\emailAdd{h.li@cern.ch}



\abstract{The ATLAS experiment at the Large Hadron Collider (LHC) utilises a trigger system consisting of a first level (Level-1) trigger based on custom hardware and a higher level trigger based on computer farms. The Level-1 muon trigger system selects muon candidates with six thresholds of transverse momentum and associate them with correct LHC bunch crossings. The Level-1 Muon Barrel Trigger uses Resistive Plate Chamber (RPC) detectors to form trigger decisions in the region of $|\eta | <$ 1.05. The RPC detectors are arranged in three concentric double layers and consist of 3700 gas volumes, with a total surface of more than 4000 square meters, operating in a toroidal magnetic field. The performance of the RPC detector system and of the Level-1 Muon Barrel trigger during the 2018 data-taking period is discussed in this contribution. 60.8 fb$^{-1}$ out of total 62.2 fb$^{-1}$ data recorded by the ATLAS detector at LHC are used. Measurements of the RPC detector response and time resolution, obtained using muon candidates produced in LHC collisions, are presented. Trigger performance and efficiency are measured with muons originating from the decay of $Z$ bosons. Finally, studies of the RPC detector response at different high voltage and threshold settings are shown, as a preview of the expected detector performance after the High Luminosity LHC upgrade.}



\keywords{Resistive Plate Chambers, Detector, Trigger, Performance}


%\arxivnumber{1234.56789} % only if you have one


% \collaboration{\includegraphics[height=17mm]{example-image}\\[6pt]
%   XXX collaboration}
% or
\collaboration[c]{on behalf of the ATLAS Muon collaboration}


% if you write for a special issue this may be useful
\proceeding{15$^{\text{th}}$ Workshop on Resistive Plate Chambers and Related Detectors - RPC2020\\
  10 - 14 February, 2020\\
  Rome, Italy}



\begin{document}
\maketitle
\flushbottom

\section{Introduction}
\label{sec:intro}
ATLAS \cite{ATLASTDR} is a general purpose particle detector at the Large Hadron Collider (LHC).
The first level (Level-1) Muon Barrel trigger \cite{L1MuonTrigger} is one of the main elements of the online event selection of the ATLAS experiment and it exploits the Resistive Plate Chamber (RPC) detectors.
As shown in Figure~\ref{fig:triggerintro} (a), the RPCs are placed in the barrel region of the ATLAS experiment. They are arranged in three concentric doublet layers at radius 7 m, 8 m and 10 m, immersed in a toroidal magnetic field of 0.5 - 1 Tesla.
The Level-1 Muon Barrel trigger allows to select muon candidates according to their transverse momenta. There are three low $p_T$ thresholds requiring three out of four coincidences in the two inner doublet layers and three high $p_T$ thresholds requiring low $p_T$ triggers and one out of two coincidences in the outer doublet layers. The algorithms of ATLAS Level-1 Muon Barrel trigger are depicted in Figure~\ref{fig:triggerintro} (b). 

\begin{figure}[htb]
\centering
\subfloat[]{\includegraphics[width=.5\textwidth]{figure/towerex}}
\qquad
\subfloat[]{\includegraphics[width=.44\textwidth]{figure/triggerlogic}}
\caption{\label{fig:triggerintro} (a) shows the XY view of the ATLAS Level-1 Muon Barrel trigger system. (b) illustrates the requirements of low and high $p_T$ thresholds of the trigger algorithms \cite{L1MuonTrigger}.}
\end{figure}


%\begin{figure}[htbp]
%\centering % \begin{center}/\end{center} takes some additional vertical space
%\includegraphics[width=.45\textwidth]{figure/towerex}
%\qquad
%\includegraphics[width=.45\textwidth]{figure/triggerlogic}
%% "\includegraphics" from the "graphicx" permits to crop (trim+clip)
%% and rotate (angle) and image (and much more)
%\end{figure}

The ATLAS RPC detector structure is shown in Figure~\ref{fig:detectorintro}.
One RPC single layer consists of parallel resistive bakelite plates which are separated by a 2 mm gas gap with insulating spacers.
%Gas mixture of tetrafluorethane $C_2H_2F_4$(94.7\%), iso-butane $C_4H_{10}$(5.0\%), sulphur hexafluoride $SF_6$(0.3\%) operated in avalanche mode applying a nominal HV of 9.6 kV 
The gas is a mixture of the $\textrm{C}_2\textrm{H}_2\textrm{F}_4$ (94.7\%, as ionization gas), $\textrm{C}_4\textrm{H}_{10}$ (5.0\%, as quencher gas) and $\textrm{SF}_6$ (0.3\%, as electronegative gas) which features relatively low operating voltage, non-flammability and low cost while operated in safe avalanche mode applying a nominal high voltage (HV) of 9.6 kV.
Muons ionize the gas and produce an avalanche. The electrons in the avalanche induce signal in the readout strips when they drift to the plate under the effect of the electric field.
The detector response is processed with the front-end circuit that includes a three-stage shaping amplifier followed by a comparator.
% C2H2F4 is to be ionised by incident particle, 
% i-C4H10 is quencher gas to absorb soft X-ray which can induce streamer mode
% SF6 is an electronegative gas to prevent too high multiplication process which will result in streamer mode
Orthogonal readout strips with 23-35 mm width are placed on both sides of the gas gaps grouped in two ($\eta$ and $\phi$) readout panels to collect the induced currents generated by the charge avalanches.
The RPC detector has excellent intrinsic time resolution of about 1 ns \cite{RPCGIFTest} which is more than sufficient to identify the proton bunches separated by 25 ns at LHC.
One RPC doublet consists of two detector layers with two $\eta$ and two $\phi$ readout panels.
The whole system has a total surface of about 4000 m$^2$ and 3600 gas volumes with 380k readout channels, covering the pseudo-rapidity range |$\eta$| < 1.05 ($\ang{38} < \theta < \ang{142}$) \cite{L1MuonTrigger, RPC2014Proceeding}.
%Besides providing trigger, RPC is the only system in the barrel Muon Spectrometer that provides the $\phi$ coordinate of the muon tracks


\begin{figure}[htbp]
\centering % \begin{center}/\end{center} takes some additional vertical space
\subfloat[]{\includegraphics[width=.58\textwidth]{figure/rpcChamber}}
\qquad
\subfloat[]{\includegraphics[width=.35\textwidth]{figure/ATLASRPC_doublet.png}}
\caption{\label{fig:detectorintro} The structure of RPC detector single layer (a) \cite{phase2TDR} and of the RPC detector doublet layer (b) \cite{RPCDoublet}.}
\end{figure}


\section{Performance of the RPC detector}

The RPC detector response is measured with muons produced in proton-proton collisions.
Muon candidates are reconstructed primarily with Monitored Drift Tube (MDT) detectors \cite{ATLASTDR} and the $\phi$ coordinate is measured by the RPC.
Muon tracks are extrapolated from MDTs, considering the magnetic field and material effects to the RPC surface to obtain the expected muon position in the RPC. Figure~\ref{fig:extrares} shows the schematic view of the muon extrapolation to the RPC surface.

\begin{figure}[htbp]
\centering % \begin{center}/\end{center} takes some additional vertical space
\includegraphics[width=.8\textwidth]{figure/resspace}
\caption{\label{fig:extrares} Schematic view of the muon extrapolation to RPC surface.}
\end{figure}

The distance between the extrapolated muon position and the center of the closest strip with a signal, referred to as "hit" hereafter, has a uniform distribution with a width of the strip pitch indicating the good RPC alignment as shown in Figure~\ref{fig:hittimeres} (a).
The arrival time of the hits after an online calibration \cite{L1MuonTrigCalibration}, relative to the triggered bunch crossing (BC), peaks near 0 with a negligible fraction out of 1 BC as shown in Figure~\ref{fig:hittimeres} (b).
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=.47\textwidth]{figure/detector/fig_5a_ClosestHitResiOverlapEta}}
\qquad
\subfloat[]{\includegraphics[width=.47\textwidth]{figure/detector/fig_4a_TimeResiOverlapEta}}
\caption{\label{fig:hittimeres} (a) Distribution of the distances between the expected $\eta$ position of muons and the center of the closest $\eta$ strip with signal in one RPC detector. (b) Online calibrated time of hits in one RPC detector, $\eta$ side view \cite{L1MuonTrigPubResults2018}.}
\end{figure}

The hit multiplicity in response to a muon passage is shown in Figure~\ref{fig:hitmuldeteffone} (a) for three selections of hits: all hits, in-time hits (hits with time of arrival within a window of $\pm$12.5 ns with respect to the BC) and signal hits (in-time hits that are within $\pm$30 mm from the extrapolated muon hit position on the strip).
The RPC panel efficiency is computed as the fraction of muons that generate a hit from the readout panel. Panel efficiencies of the RPC detector calculated using the three hit selection criteria have a difference less than 1\%, which illustrates typical good timing performance and efficiency of RPC panels.
The efficiency calculated using the signal hits for $\eta$ and $\phi$ views of this RPC detector, for each ATLAS run recorded in 2018, shown in Figure~\ref{fig:hitmuldeteffone} (b), is stable at approximately 96\%.
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=.49\textwidth]{figure/detector/fig_3a_HitMultiOverlapEta}}
\qquad
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/detector/fig_12_PanelEffTime}}
\caption{\label{fig:hitmuldeteffone} (a) Hit multiplicity in response to a muon passage in one RPC detector, $\eta$ side view. (b) Efficiency for one RPC detector in $\eta$ and $\phi$ views, for each ATLAS run recorded in 2018 \cite{L1MuonTrigPubResults2018}.}
\end{figure}

The panel and gap efficiency distributions of all operational detectors which include about 90\% of all the RPC detectors in one run are shown in Figure~\ref{fig:deteffall} (a) and the mean detector efficiency in each run recorded in 2018 is shown in Figure~\ref{fig:deteffall} (b). Most of the detectors show a high efficiency.
The gap efficiency defined by hits registered for at least one of $\eta$ and $\phi$ coordinates is about 94\% on average. The overall detector efficiency is stable at around 90\% during 2018. A small increase in the average efficiency observed in August is due to an intervention on the RPC Detector Control System (DCS), during which the front-end electronics thresholds have been re-tuned.
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=.49\textwidth]{figure/detector/fig_11_PanelGapEff}}
\qquad
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/detector/fig_13_MeanPanelEffTime}}
\caption{\label{fig:deteffall} (a) Distribution of the panel and gap efficiencies for all operational RPC detectors in one ATLAS run in 2018. (b) Mean efficiency for $\eta$ and $\phi$ panels of all operational RPC detectors in each run recorded in 2018 \cite{L1MuonTrigPubResults2018}.}
\end{figure}

\section{Performance of the Level-1 Muon Barrel trigger}

A correct BC association is one of the main requirements of the Level-1 muon barrel trigger. The RPC hit time online calibration is loaded in trigger electronics and used for online data-taking \cite{L1MuonTrigCalibration}. It is performed in steps of 3.125 ns which is one eighth of the LHC BC so that this precision is sufficient to identify the correct BC.
Overall, 99.6\% to 99.7\% of the RPC hits that are associated with the muons in the trigger signature correspond to the correct BC. Good timing calibration stability is observed during the full data-taking period, except a few outliers due to a trigger tower losing synchronization during a run, as shown in Figure~\ref{fig:trigtime}.
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/trigger/fig_07}}
\qquad
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/trigger/BC0HighPtFractionTime}}
\caption{\label{fig:trigtime} (a) Global fraction of trigger hits per bunch crossing for data 2015 \cite{L1MuonTrigPubResults2015}. (b) Fraction of trigger hits in the correct bunch crossing for each ATLAS run recorded in 2018 \cite{L1MuonTrigPubResults2018}.}
\end{figure}

Efficiency is a key parameter of the trigger system. The Level-1 muon barrel trigger efficiency is measured using muons from $Z\rightarrow\mu^{\pm}\mu^{\mp}$ candidate events, exploiting the tag-and-probe method to remove the trigger bias \cite{ZTPunbias}. The efficiency as a function of the offline muon $p_T$ for six $p_T$ thresholds is shown in Figure~\ref{fig:trigeff} (a).
The Level-1 muon trigger efficiency is reduced in the barrel region by the loss of acceptance due to support structures of toroidal magnets and ATLAS "feet".
The efficiency, including the acceptance effects, to detect muons with $p_T$ > 20 GeV is about 76.5\% for low $p_T$ thresholds and 70.0\% for high $p_T$ thresholds as observed in the plateau. The plateau value in each ATLAS run recorded in 2018 shows very good stability during the whole year, as shown in Figure~\ref{fig:trigeff} (b).
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/trigger/efficiency_vs_pt}}
\qquad
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/trigger/TriggerEffTime}}
\caption{\label{fig:trigeff} (a) Trigger efficiency for offline muons as a function of the transverse momentum. (b) Plateau value of the trigger efficiency in each ATLAS run recorded in 2018 \cite{L1MuonTrigPubResults2018}.}
\end{figure}

\section{Efficiency with lowered HV and thresholds}

A few studies are performed to preview operations of upgraded RPC detector \cite{phase2TDR} for the High-Luminosity LHC (HL-LHC). At the HL-LHC, the instantaneous luminosity will reach about $7.5\times10^{34}~\textrm{cm}^{-2}\textrm{s}^{-1}$ which is a factor of three higher than the last round of data-taking.
To improve the longevity of the RPC detectors, the HV applied on RPC detectors will be lowered from 9.6 kV to 9.2 kV; at the same time, to recover the selectivity and efficiency of the barrel muon triggers, new triplet RPCs will be installed in the innermost layer of the Muon Barrel Spectrometer \cite{phase2TDR}.
The response of a few RPC detectors at nominal and lower HV with different front end (FE) discriminator thresholds ($V_\textrm{FE}$) are measured. Setting $V_\textrm{FE}$ to 1 V corresponds to the nominal threshold value, while setting $V_\textrm{FE}$ to 1.1 V or 1.2 V corresponds to applying softer threshold values to the FE board discriminator \cite{L1MuonTrigger}.
Figure~\ref{fig:Vthscan} presents the detector efficiency as a function of $V_\textrm{FE}$. The efficiency trend at the nominal HV is quite flat, while at the lower HV, the efficiency increases along with lowering the $V_\textrm{FE}$, which indicates that part of the efficiency lost in reducing the HV is recovered by lowering the $V_\textrm{FE}$. The efficiency gain defined as the efficiency increase from $V_\textrm{FE}$ = 1.0 V to 1.2 V is about 20\% in the best case and 10\% on average.
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/detector/fig_14a_VFEScan}}
\qquad
\subfloat[]{\includegraphics[width=.45\textwidth]{figure/detector/fig_15a_VFEScan}}
\caption{\label{fig:Vthscan} The efficiency of one RPC detector at nominal and lower HV with different FE thresholds for a case with high efficiency gain (a) and a case with average efficiency gain (b) \cite{L1MuonTrigPubResults2018}.}
\end{figure}


\section{Conclusion}

Muon barrel RPC triggers selecting muon candidates at 40 MHz collision rate are crucial for the ATLAS experiment.
Detailed studies have been performed to monitor the RPC performance continuously during the year 2018.
The ATLAS RPCs are working with excellent efficiency, in both detecting and triggering on muons, since the beginning of the data-taking, even when the instantaneous luminosity is higher than the design by a factor of 2.
Preliminary studies indicate that the existing ATLAS RPCs will perform well at higher instantaneous luminosity.
Extensive detector upgrades to prepare for High Luminosity LHC including a new RPC inner triplet layer and new readout electronics are ongoing.

%\clearpage
\acknowledgments
I am indebted to CERN for the very successful operation of the LHC, as well as the support staff from my institute and ATLAS collaboration, especially, Rustem Ospanov, Yanwen Liu, Marco Sessa, Zuzana Blenessy, Giulio Aielli, Claudio Luci and Massimo Corradi for many helpful suggestions.

%Copyright 2020 CERN for the benefit of the ATLAS Collaboration. Reproduction of this article or parts of it is allowed as specified in the CC-BY-4.0 license.

% We suggest to always provide author, title and journal data:
% in short all the informations that clearly identify a document.

\begin{thebibliography}{99}

\bibitem{ATLASTDR}
ATLAS Collaboration, \emph{The ATLAS experiment at the CERN Large Hadron Collider}, \href{https://iopscience.iop.org/article/10.1088/1748-0221/3/08/S08003}{JINST {\bf 3} (2008) S08003}.

\bibitem{L1MuonTrigger}
F. Anulli et al., \emph{The Level-1 Trigger Muon Barrel System of the ATLAS experiment at CERN}, \href{https://iopscience.iop.org/article/10.1088/1748-0221/4/04/P04010}{JINST {\bf 4} (2009) P04010}.

\bibitem{RPCGIFTest}
G. Aielli et al., \emph{Performance of a large-size RPC equipped with the final ATLAS front-end electronics at X5GIF irradiation facility}, \href{https://www.sciencedirect.com/science/article/pii/S0168900200009669?via\%3Dihub}{Nucl. Instrum. Meth. PHYS. RES. A {\bf 456} (2000) 77-81}.

\bibitem{RPC2014Proceeding}
D. Boscherini, \emph{Performance and operation of the {ATLAS} Resistive Plate Chamber system in {LHC} Run-1}, \href{https://iopscience.iop.org/article/10.1088/1748-0221/9/12/C12039}{JINST {\bf 9} (2014) C12039}.

\bibitem{phase2TDR}
ATLAS Collaboration, \emph{Technical Design Report for the Phase-II Upgrade of the ATLAS Muon Spectrometer}, CERN-LHCC-2017-017 (2017), \url{https://cds.cern.ch/record/2285580}.

\bibitem{RPCDoublet}
G. Aielli et al., \emph{The RPC LVL1 trigger system of the muon spectrometer of the ATLAS experiment at LHC}, \href{https://ieeexplore.ieee.org/document/1323734}{IEEE Transactions on Nuclear Science {\bf 51} (2004) 1581-1589}.

\bibitem{L1MuonTrigCalibration}
Ciapetti, G. et al, \emph{The ATLAS barrel level-1 muon trigger calibration}, ATL-DAQ-CONF-2007-020 (2007), \url{https://cds.cern.ch/record/1035895}.

\bibitem{L1MuonTrigPubResults2018}
ATLAS Collaboration, \emph{Level 1 muon barrel trigger performance in 2018}, \url{https://twiki.cern.ch/twiki/bin/view/AtlasPublic/L1MuonTriggerPublicResults}.

\bibitem{L1MuonTrigPubResults2015}
ATLAS Collaboration, \emph{Level 1 Barrel Muon trigger and RPC performance in 2015}, \url{https://twiki.cern.ch/twiki/bin/view/AtlasPublic/L1MuonTriggerPublicResults}.

\bibitem{ZTPunbias}
ATLAS Collaboration, \emph{Performance of the ATLAS Trigger System in 2015}, \href{https://link.springer.com/article/10.1140/epjc/s10052-017-4852-3}{Eur. Phys. J. C {\bf 77} (2017) 317}.


% Please avoid comments such as "For a review'', "For some examples",
% "and references therein" or move them in the text. In general,
% please leave only references in the bibliography and move all
% accessory text in footnotes.

% Also, please have only one work for each \bibitem.


\end{thebibliography}
\end{document}
